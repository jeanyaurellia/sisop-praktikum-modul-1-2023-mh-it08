# Soal Shift SisOp Modul 1

## Soal no. 1

#### 1.1 `wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp' -O 'playlist.csv'`

Pada line ini mendownload file **playlist.csv** dengan tidak memperhatikan sertifikasi SSH/memaksa mendownload dengan menggunakan perintah `--no-check-certificate`. Dan memasukkan ID dari drive yaitu `1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp`. `-O 'playlist.csv'` menunjukkan nama file output yang akan didownload

#### 1.2 `awk -F ',' '$4 ~/hip hop/ {print $15, $4, $2}' playlist.csv| sort -k1,1nr | head -n 5`

`awk -F ',' '$4 ~/hip hop/` ini menunjukkan jika dari file akan dicari di tabel 4 yaitu genre dengan tiap tabelnya terpisah oleh ',' (koma) yang kalimatnya terdapat kata hip hop. `{print $15, $4, $2}` bagian ini akan mengeprint kolom 15 (popularity), 4 (genre) dam 2 (lagu) dimana yang terdapat kata hip hop. kemudian perintah ini `sort -k1,1nr `digunakan untuk sort tingkat popularity dari tinggi ke rendah dengan input adalah numerik. Dan akan diambil 5 lagu dengan popularity terbesar dengan `head -n 5`.

#### 1.3`awk -F ',' '$3 =="John Mayer" {print $15, $3, $2}' playlist.csv| sort -k1,1nr | head -n 5`

Pada bagian ini `'$3 =="John Mayer"` akan disort pada kolom 3 (artist) yang terdapat nama artist John Mayer. Kemudian `{print $15, $3, $2}` akan diprint popularity (15), artist (3), lagu (2) yang terdapat nama John Mayer. `sort -k1,1nr` akan melakukan sorting terdapat tabel awk yang di print sebelumnya dimana `-k1` menandakan jika kolom 1 yaitu popularity akan disort numeriknya secara terbalik (dari besar ke kecil) dengan `1nr`. Dan akan diambil 5 lagu dengan popularity terbesar `head -n 5`

#### 1.4`awk -F ',' '$5 =="2004" {print $15, $5, $2}' playlist.csv| sort -k1,1nr | head -n 10`

Line `$5 =='2004'` akan mencari pada kolom 5 (tahun) yang memiliki kata 2004. Kemudian di print kolom 15 (popularity), 5 (tahun), dan lagu (2) yang terdapat kata 2004 dengan `{print $15, $5, $2}`. Akan di sort kolom 1 (dari awk yang di print) secara terbalik `sort -k1,1nr`. Dan akan diambil 10 lagu dengan popularity terbesar `head -n 10`

#### 1.5`grep "Sri" playlist.csv | cut -d ',' -f 3,2`

Line `grep "Sri" playlist.csv` digunakan untuk mencari tabel yang memuat kata "Sri". Kemudia akan diprint kolom 3 (artist) dan 2 (lagu) yang memuat kata "Sri" dengan `cut -d ',' -f 3,2`.

### Output no 1
![App Screenshot](https://i.ibb.co/ZT5sV9H/soal1.png)

## Soal no. 2

### 1. Penjelasan register.sh
```bash
#!/bin/bash

user=$(whoami)
path=/home/${user}/users
time=$(date +"%Y/%m/%d %H:%M:%S")

masuk=true


while $masuk
do
    echo "Masukkan email: "
    read email

    echo "Masukkan username: "
    read username

    echo "Masukkan password: "
    read password

    emailData=$(cat $path/users.txt | cut -d "," -f 1 | tr '\n' ' ')

    IFS=' ' read -a emailArr <<< "$emailData"

    emailTrue=true

    for item in "${emailArr[@]}"
    do 
        if [ "$item" == "$email" ]
        then
            echo "Email sudah terdaftar"
            emailTrue=false
            break
        fi
    done

    pw=true

    if [[ ${#password} -lt 8 ]]; then
        echo "Password harus tepat 8 karakter"
        pw=false
    fi

    if [[ ! ${password} =~ [A-Z] ]]; then
        echo "Password tidak memiliki huruf besar"
        pw=false
    fi

    if [[ ! ${password} =~ [a-z] ]]; then
        echo "Password tidak memiliki huruf kecil"
        pw=false
    fi

    if [[ ! ${password} =~ [0-9] ]]; then
        echo "Password tidak memiliki angka"
        pw=false
    fi

    if [[ ! ${password} =~ [^a-zA-Z0-9\s] ]]; then
        echo "Password tidak memiliki special characters"
        pw=false
    fi


    if $emailTrue
    then
        echo "Email Teregistrasi"
    fi

    if $emailTrue && $pw 
    then
        encoded_password=$(echo -n "$password" | base64)
        echo "$email,$username,$encoded_password" >> $path/users.txt
        echo " " >> $path/users.txt
        echo "[$time] [REGISTER SUCCESS] user $username registered successfully" >> $path/auth.log
        echo "Berhasil Registrasi"
        masuk=false
    else
        echo "Password tidak sesuai ketentuan"
        echo "[$time] [REGISTER FAILED] ERROR Failed register attempt on user" >> $path/auth.log
    fi
done
```

#### 1.1 Initalisasi variabel di awal
Dilakukan inisialisasi beberapa variabel di awal seperti `user`, `time`, dan `path` untuk memudahkan pembuatan program. `masuk` digunakan untuk memberhentikan perulangan while bila user sudah mendaftarkan akun.

#### 1.2 Perulangan `while`
Perulangan `while` disini dilakukan agar jika pengguna tidak melakukan proses pendaftaran dengan benar maka program didalam section `while` ini akan berjalan kembali.

#### 1.3 Output dan input
Berikutnya ada `echo "Masukkan email: "` dan sejenisnya digunakan untuk memberikan arahan kepada pengguna untuk memasukkan data yang sesuai. Sedangkan `read username` digunakan untuk membaca input dari pengguna.

#### 1.4 `emailData=$(cat $path/users.txt | cut -d "," -f 1 | tr '\n' ' ')`
Digunakan untuk mengambil list semua email yang sudah terdaftar.

##### 1.4.1 `cat $path/users.txt`
Digunakan untuk membuka isi file dimana tempat menyimpan semua daftar registrasi

##### 1.4.2 `cut -d "," -f 1`
Digunakan untuk memisahkan output yang didapatkan dari program sebelumnya bedasarkan `,` dan mengambil field yang diperlukan, yang pada kondisi ini mengambil data email yang berada pada field bagian 1.

##### 1.4.3 `tr '\n' ' '`
Digunakan karena output dari command sebelumnya belum dalam satu baris, hal ini akan menjadi suatu masalah dikarenakan nantinya nilai ini akan dijadikan sebuah array, untuk itu akan diganti yang sebelumnya baris selanjutnya (`\n`) ke dalam satu baris yang sama (`' '`).

#### 1.5 `IFS=' ' read -a emailArr <<< "$emailData"`
Line ini diperlukan agar variabel `emailData` (yang dimana masih berupa string) diubah ke bentuk array agar bisa dilakukan pengecekan email yang sudah terdaftar. Bentuk array tersebut disimpan di variabel `emailArr`.

#### 1.6

### Output no 2
![App Screenshot](https://i.ibb.co/zXCyrfn/soal2.png)


## Soal no. 3

### Output no 3 genshin.sh
![App Screenshot](https://i.ibb.co/4FFJj71/soal3genshin1.png)


![App Screenshot](https://i.ibb.co/4FFJj71/soal3genshin2.png)


### Output no 3 find_me.sh
![App Screenshot](https://i.ibb.co/zhvpK10/soal3findme1.png)


![App Screenshot](https://i.ibb.co/pjcrGNN/soal3findme2.png)


![App Screenshot](https://i.ibb.co/N6tfzHY/soal3findme3.png)
## Soal no. 4

### 1. Penjelasan minute_log.sh
```bash
#!/bin/bash

timestamp=$(date "+%Y%m%d%H%M%S")
user=$(whoami)
path="/home/${user}/log"

memTot=$(free -m | grep Mem | awk '{print $2}')
memUse=$(free -m | grep Mem | awk '{print $3}')
memFre=$(free -m | grep Mem | awk '{print $4}')
memBgi=$(free -m | grep Mem | awk '{print $5}')
memCac=$(free -m | grep Mem | awk '{print $6}')
memAva=$(free -m | grep Mem | awk '{print $7}')

swapTot=$(free -m | grep Swap | awk '{print $2}')
swapUse=$(free -m | grep Swap | awk '{print $3}')
swapFre=$(free -m | grep Swap | awk '{print $4}')

checkPath="/home/${user}"
sizePath=$(du -sh ${checkPath}| awk '{print $1}')

echo $memTot,$memUse,$memFre,$memBgi,$memCac,$memAva,$swapTot,$swapUse,$swapFre,$checkPath,$sizePath > ${path}/metrics_${timestamp}.log

chmod 600 ${path}/metrics_${timestamp}.log
```

#### 1.1. `timestamp=$(date "+%Y%m%d%H%M%S")`.
Line ini ditujukan untuk menyimpan waktu saat ini. `"+%Y%m%d%H%M%S"` bertujuan untuk merubah formatnya ke YYYYmmddHHMMSS.

#### 1.2. `user=$(whoami)`.
Bertujuan untuk menyimpan dan mengetahui nama user yang menjalankan program ini (`whoami`) dan menyimpannya di variabel `user`

#### 1.3. `path="/home/${user}/log"`.
Bertujuan untuk menyimpan lokasi tempat menyimpan file log yang di keluarkan oleh program ini

#### 1.4 `memTot=$(free -m | grep Mem | awk '{print $2}')` dan sejenisnya.
Line ini bertujuan untuk mengambil nilai-nilai memory yang diperlukan dan menyimpan di varibel yang sesuai. 

##### 1.4.1. `free -m`.
Digunakan untuk menampilkan data memmory. 

##### 1.4.2. `grep Mem`. 
Digunakan untuk hanya mengambil row `Mem`. 

##### 1.4.3. `awk '{print $2}')`.
Bertujuan untuk mengambil value di kolom 2 yaitu `total memory` dan mengoutputkannya. Output dari program tersebut akan disimpan di variabel `memTot` untuk digunakan nanti. Rumus line ini juga digunakan untuk mengambil value-value lain seperti `free memory`, `available memory`, `swap memory total`, dan lain-lain.

#### 1.5. `checkPath="/home/${user}"`.
Line ini diguankan untuk menyimpan lokasi directory yang ingin dicek sizenya.

#### 1.6. `sizePath=$(du -sh ${checkPath}| awk '{print $1}')`.
Line ini digunakan untuk mengetahui berapa size yang ada di directory tersebut, mengambil nilainya, dan menyimpannya di variabel `sizePath`. 

##### 1.6.1. `du -sh ${checkPath}`.
Digunakan untuk mengetahui berapa size di directory tersebut, digunakan `${checkPath}` untuk lebih memudahkan penulisan dan pembacaan kode. 

##### 1.6.2. `awk '{print $1}'`.
Digunakan untuk mengambil value di kolom pertama, yaitu size directory tersebut, dan mengoutputkannya.

#### 1.7. `echo $memTot,$memUse,$memFre,$memBgi,$memCac,$memAva,$swapTot,$swapUse,$swapFre,$checkPath,$sizePath > ${path}/metrics_${timestamp}.log`.
Line ini diguankan untuk mengoutputkan semua varibel yang diperlukan dan menyimpannya di file log yang diperintahkan, yaitu `metrics_{YmdHms}.log`. Disini variabel `timestamp` dan variabel `path` yang di buat di atas digunakan untuk memudahkan pembuatan program.

#### 1.8 `chmod 600 ${path}/metrics_${timestamp}.log`.
Digunakan agar file log hanya bisa dibaca dan diedit oleh pemilik file.

### Output no 4 minute_log.sh
![App Screenshot](https://i.ibb.co/kGmrQwk/Screenshot-from-2023-09-30-07-34-51.png)

### 2. Penjelasan aggregate_minutes_to_hourly_log.sh
```bash
#!/bin/bash

timestamp=$(date "+%Y%m%d%H%M%S")
user=$(whoami)
path="/home/${user}/log"
checkPath="/home/${user}"


memTot=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 1 | tr '\n' ' ')
memUse=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 2 | tr '\n' ' ')
memFre=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 3 | tr '\n' ' ')
memBgi=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 4 | tr '\n' ' ')
memCac=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 5 | tr '\n' ' ')
memAva=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 6 | tr '\n' ' ')

swapTot=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 7 | tr '\n' ' ')
swapUse=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 8 | tr '\n' ' ')
swapFre=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 9 | tr '\n' ' ')

pathSize=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 11 | tr '\n' ' ' | tr 'M' ' ')


IFS=' ' read -a memTotArr <<< "$memTot"
IFS=' ' read -a memUseArr <<< "$memUse"
IFS=' ' read -a memFreArr <<< "$memFre"
IFS=' ' read -a memBgiArr <<< "$memBgi"
IFS=' ' read -a memCacArr <<< "$memCac"
IFS=' ' read -a memAvaArr <<< "$memAva"

IFS=' ' read -a swapTotArr <<< "$swapTot"
IFS=' ' read -a swapUseArr <<< "$swapUse"
IFS=' ' read -a swapFreArr <<< "$swapFre"

IFS=' ' read -a pathSizeArr <<< "$pathSize"


avg() {
    local arr=("$@")
    local sum=0
    for item in "${arr[@]}"; do
        item_int=$((item))
        sum=$((sum+item_int))
    done
    avg=$((sum/60))
    echo "$avg"
}


min() {
    local arr=("$@")
    local min=${arr[0]}  
    for item in "${arr[@]}"; do
        item_int=$((item))
        if [ "$item_int" -lt "$min" ]; then
            min=$item_int
        fi
    done
    echo "$min"
}


max() {
    local arr=("$@")
    local max=${arr[0]}  
    for item in "${arr[@]}"; do
        item_int=$((item))
        if [ "$item_int" -gt "$max" ]; then
            max=$item_int
        fi
    done
    echo "$max"
}


minMemTot=$(min "${memTotArr[@]}")
maxMemTot=$(max "${memTotArr[@]}")
avgMemTot=$(avg "${memTotArr[@]}")

minMemUse=$(min "${memUseArr[@]}")
maxMemUse=$(max "${memUseArr[@]}")
avgMemUse=$(avg "${memUseArr[@]}")

minMemFre=$(min "${memFreArr[@]}")
maxMemFre=$(max "${memFreArr[@]}")
avgMemFre=$(avg "${memFreArr[@]}")

minMemBgi=$(min "${memBgiArr[@]}")
maxMemBgi=$(max "${memBgiArr[@]}")
avgMemBgi=$(avg "${memBgiArr[@]}")

minMemCac=$(min "${memCacArr[@]}")
maxMemCac=$(max "${memCacArr[@]}")
avgMemCac=$(avg "${memCacArr[@]}")

minMemAva=$(min "${memAvaArr[@]}")
maxMemAva=$(max "${memAvaArr[@]}")
avgMemAva=$(avg "${memAvaArr[@]}")


minSwapTot=$(min "${swapTotArr[@]}")
maxSwapTot=$(max "${swapTotArr[@]}")
avgSwapTot=$(avg "${swapTotArr[@]}")

minSwapUse=$(min "${swapUseArr[@]}")
maxSwapUse=$(max "${swapUseArr[@]}")
avgSwapUse=$(avg "${swapUseArr[@]}")

minSwapFre=$(min "${swapFreArr[@]}")
maxSwapFre=$(max "${swapFreArr[@]}")
avgSwapFre=$(avg "${swapFreArr[@]}")


minPathSize=$(min "${pathSizeArr[@]}")
maxPathSize=$(max "${pathSizeArr[@]}")
avgPathSize=$(avg "${pathSizeArr[@]}")

echo "minimum,$minMemTot,$minMemUse,$minMemFre,$minMemBgi,$minMemCac,$minMemAva,$minSwapTot,$minSwapUse,$minSwapFre,$checkPath,${minPathSize}M" >> $path/metrics_agg_$timestamp.log
echo "maximum,$maxMemTot,$maxMemUse,$maxMemFre,$maxMemBgi,$maxMemCac,$maxMemAva,$maxSwapTot,$maxSwapUse,$maxSwapFre,$checkPath,${maxPathSize}M" >> $path/metrics_agg_$timestamp.log
echo "average,$avgMemTot,$avgMemUse,$avgMemFre,$avgMemBgi,$avgMemCac,$avgMemAva,$avgSwapTot,$avgSwapUse,$avgSwapFre,$checkPath,${avgPathSize}M" >> $path/metrics_agg_$timestamp.log

chmod 600 $path/metrics_agg_$timestamp.log
```

#### 2.1. `timestamp`,`user`,`path`, etc.
Penggunaan line ini sama seperti pada file  minute_log.sh. Silahkan melihat [minute_log.sh](#1-penjelasan-minute_logsh) untuk detail lebih lanjut.

#### 2.2. `memTot=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 1 | tr '\n' ' ')` dan sejenisnya.
Line ini diperlukan untuk menjalankan program `ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 1 | tr '\n' ' '` dan lain-lainnya, dan menyimpannya ke variabel yang sesuai. 

##### 2.2.1. `ls /home/${user}/log`.
Digunakan untuk melist semua file yang ada di directory `/home/${user}/log`. 

##### 2.2.2. `grep -Ev agg`.
Digunakan untuk hanya mengambil output yang tidak memiliki kata `agg`, hal ini dperlukan karena di dalam directory `log` juga ada file agresasi perjam dan seharusnya program ini tidak perlu menggunakan dari file tersebut. 

##### 2.2.3. `sort | tail -n 60`.
Digunakan untuk mengurutkan file-file tersebut dari yang paling lama ke yang paling terbaru, kemudian dengan `tail -n 60` akan diambil 60 file yang paling baru. 

##### 2.2.4. `xargs -I {} cat /home/${user}/log/{}`.
Digunakan untuk mengambil output dari command sebelumnya (`xargs -I {}`) dan membaca file tersebut (`cat /home/${user}/log/{}`). 

##### 2.2.5. `cut -d "," -f 1`.
Digunakan untuk memisahkan output yang didapatkan dari program sebelumnya bedasarkan `,` dan mengambil field yang diperlukan, contohnya untuk memory total akan diambil field yang pertama (`-f 1`) karena di program `minute_log.sh` memory total di tulis di yang pertama. 

##### 2.2.6. `tr '\n' ' '`.
Digunakan karena output dari command sebelumnya belum dalam satu baris, hal ini akan menjadi suatu masalah dikarenakan nantinya nilai ini akan dijadikan sebuah array, untuk itu akan diganti yang sebelumnya baris selanjutnya (`\n`) ke dalam satu baris yang sama (`' '`). Pada variabel `pathSize` ada penambahan command `tr 'M' ' '` dikarenakan nilai untuk `pathSize` masih memiliki huruf `M` di akhirannya dan diperlukan untuk menghapus huruf tersebut agar bisa dilakukan perhitungan.


#### 2.3. `IFS=' ' read -a memTotArr <<< "$memTot"` dan sejenisnya.
Line ini diperlukan agar variabel `memTot` dan lain-lainnya (yang dimana masih berupa string) diubah ke bentuk array agar bisa dilakukan perhitungan. Bentuk array tersebut disimpan di variabel `memTotArr`.

#### 2.4. Fungsi `avg()`.
Fungsi `avg()` ini diperlukan untuk mencari nilai rata-rata dari semua data yang dimiliki
```bash
avg() {
    local arr=("$@")
    local sum=0
    for item in "${arr[@]}"; do
        item_int=$((item))
        sum=$((sum+item_int))
    done
    avg=$((sum/60))
    echo "$avg"
}
```

##### 2.4.1. `local arr=("$@)`.
Digunakan untuk menjadikan variabel `arr` sebagai tempat menyimpan argumen yang akan digunakan untuk fungsi ini.

##### 2.4.2. `local sum=0`.
Digunakan untuk menyimpan jumlah semua nilai yang ada di variabel `arr`.

##### 2.4.3. Perulangan `for` dengan varibel `"${arr[@]}"`.
```bash
for item in "${arr[@]}"; do
    item_int=$((item))
    sum=$((sum+item_int))
done
```
Perulangan ini digunakan untuk menjumlahkan semua item yang ada dari variabel `arr`. Dengan menggunakan perulangan ini, akan di iterate semua item yang di variabel `arr` dan akan dijalankan program yang ada di dalam perulangan `for` ini.

###### 2.4.3.1. `item_int=$((item))`.
Digunakan untuk mengubah item yang diterima ke bentuk interger.

###### 2.4.3.2. `sum=$((sum+item_int))`.
Digunakan untuk menambahkan nilai `item_int` ke variabel `sum`.

##### 2.4.4 `avg=$((sum/60))`.
Digunakan untuk mendapatkan nilai rata-rata dari argumen yang di masukkan ke fungsi ini dengan membaginya dengan banyak data yang ada (60).

##### 2.4.5 `echo "$avg"`.
Digunakan untuk mengembalikan hasil nilai rata-rata.

#### 2.5. Fungsi `min()` & `max()`.
Fungsi `min()`:
```bash
min() {
    local arr=("$@")
    local min=${arr[0]}  
    for item in "${arr[@]}"; do
        item_int=$((item))
        if [ "$item_int" -lt "$min" ]; then
            min=$item_int
        fi
    done
    echo "$min"
}
```
Fungsi `max()`:
```bash
max() {
    local arr=("$@")
    local max=${arr[0]}  
    for item in "${arr[@]}"; do
        item_int=$((item))
        if [ "$item_int" -gt "$max" ]; then
            max=$item_int
        fi
    done
    echo "$max"
}
```
Beberapa command di fungsi ini memiliki kesamaan di fungsi `avg()`. Untuk mengetahui lebih lanjut silahkan melihat [Fungsi avg()](#24-fungsi-avg).

##### 2.5.1 Fungsi konditional di dalam `for` loop.
Pada fungsi `min()` dan `max()`, terdapat fungsi konditional `if`. Pada fungsi `min()` digunakan kondisi `[ "$item_int" -lt "$min" ]` dimana `-lt` ini adalah `<` untuk bahasa bash. Jika kondisi terpenuhi, maka variabel `min` akan memiliki nilai yang lebih kecil dari yang dimiliki sebelumnya(`min=$item_int`), hal ini bertujuan untuk mendapatkan nilai terkecil dari semua data yang dimiliki. Hal ini juga berlaku untuk fungsi `max()`. Namun, fungsi konditional dalam fungsi `max()` bertujuan untuk mendapatkan nilai terbesar dari semua data yang dimiliki.

#### 2.6. `minMemTot=$(min "${memTotArr[@]}")` dan sejenisnya.
Digunakan untuk menjalankan fungsi yang diinginkan (`min()`, `max()`, atau `avg()`) dengan argumen yang diinginkan (`memTotArr`, `memFreArr`, etc.) untuk mendapatkan nilai terbesar, terkecil, atau rata-rata dari data yang ada. Sebagai contoh, `minMemTot=$(min "${memTotArr[@]}")` adalah menjalankan fungsi `min()` dengan argumen `memTotArr` dan menyimpan hasilnya di `minMemTot`.

#### 2.7. `echo "minimum,$minMemTot,$minMemUse,$minMemFre,$minMemBgi,$minMemCac,$minMemAva,$minSwapTot,$minSwapUse,$minSwapFre,$checkPath,${minPathSize}M" >> $path/metrics_agg_$timestamp.log` dan sejenisnya.
Digunakan untuk mengoutputkan data-data yang diperlukan dan menyimpannya di file `metrics_agg_$timestamp.log`.

#### 2.8. `chmod 600 $path/metrics_agg_$timestamp.log`
Digunakan agar hanya pemilik file yang dapat membaca dan mengedit file tersebut


### 3. Penjelasan file `crontab`.
```bash
* * * * * /home/ikktaa/Documents/Projects/SisOp-Day-1/minute_log.sh
0 * * * * /home/ikktaa/Documents/Projects/SisOp-Day-1/aggregate_minutes_to_hourly_log.sh
```
Untuk menjalankan `crontab` diperlukan untuk menjalankan command `crontab -e` terlebih dahulu di terminal. Lalu, akan terbuka file `crontab` yang dimana diperlukan untuk ditambahkan yang disebutkan sebelumnya. Program `minute_log.sh` harus dijalankan setiap menitnya, maka akan digunakan `* * * * *` agar tujuan tersebut tercapai. Sedangkan program `aggregate_minutes_to_hourly_log.sh` diperlukan untuk dijalankan setiap jam, maka akan digunakan `0 * * * *` agar tujuan tersebut tercapai.

