#!/bin/bash

#download dan unzip file
wget 'https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2' -O genshin.zip
unzip genshin.zip
unzip genshin_character.zip -d genshin_character
mv list_character.csv genshin_character
cd genshin_character
#untuk decode file dan me-rename file berdasarkan csv
for encoded_name in *.jpg; do
    encoded_filename="${encoded_name%.*}"
    decoded_filename=$(echo "$encoded_filename" | base64 -d)
    if [ "$decoded_filename" != "$encoded_filename" ]; then
        csv_file="list_character.csv" #csv
        matching_line=$(awk "/$decoded_filename/ {print}" "$csv_file")
        if [ -n "$matching_line" ]; then
            word2=$(echo "$matching_line" | cut -d',' -f2)
            word3=$(echo "$matching_line" | cut -d',' -f3)
            word4=$(echo "$matching_line" | cut -d',' -f4)
            new_filename="$decoded_filename - $word2 - $word3 - $word4.jpg"
            mv "$encoded_name" "$new_filename"
            # menghilangkan '\r'
            new_filename_cleaned=$(echo "$new_filename" | tr -d '\r')
            mv "$new_filename" "$new_filename_cleaned"
            echo "Renamed '$encoded_name' to '$new_filename_cleaned'"
        fi
    fi
done
# untuk memindahkan file directory susuai dengan region masing masing
source_dir=$(pwd)
for file in "$source_dir"/*.jpg; do
  if [ -f "$file" ]; then
    # mengambil kata kedua (region) dari file
    second_word=$(basename "$file" | awk -F'- ' '{printf "%s",$2}'|tr -d ' ')
    # buat variabel direktori tujuan sesuai dengan kata kedua dari yang diambil
    dest_dir="$source_dir/$second_word"
    # buat direktori sesuai dengan variabel
    mkdir -p "$dest_dir"
    # memindahkan file ke direktori tujuan
    mv "$file" "$dest_dir/"
    echo "Moved '$file' to '$dest_dir/'"
  fi
done

#Soal B menghitung berapa senjata dan menghapus yang tidak dibutuhkan
echo "[Bow] = [$(find -name "*Bow*"*".jpg" | wc -l)"]
echo "[Claymore] = [$(find -name "*Claymore*"*".jpg" | wc -l)"]
echo "[Catalyst] = [$(find -name "*Catalyst*"*".jpg" | wc -l)"]
echo "[Polearm] = [$(find -name "*Polearm*"*".jpg" | wc -l)"]
echo "[Sword] = [$(find -name "*Sword*"*".jpg" | wc -l)"]
rm list_character.csv; cd .. ; rm genshin_character.zip genshin.zip 
