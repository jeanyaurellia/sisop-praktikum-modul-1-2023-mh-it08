#!/bin/bash

time=$(date +"%Y/%m/%d %H:%M:%S")

url_regex="^(http|https|ftp)://[^\s/$.?#].[^\s]*$"

find -type f -name "*.jpg" -print0 | while IFS= read -r -d '' item
do
    echo "Extracting..."
    steghide extract -sf "${item}" -p "" -xf output.txt
    output=$(cat output.txt | base64 -d)
    if [[ "$output" =~ $url_regex ]]
    then
        echo "[$time] [FOUND] [$item]" >> image.log
        echo $output >> url.txt
        rm output.txt
        exit 0
    else
        echo "[$time] [NOT FOUND] [$item]" >> image.log
        rm output.txt
    fi
done