#!/bin/bash

timestamp=$(date "+%Y%m%d%H%M%S")
user=$(whoami)
path="/home/${user}/log"
checkPath="/home/${user}"


memTot=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 1 | tr '\n' ' ')
memUse=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 2 | tr '\n' ' ')
memFre=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 3 | tr '\n' ' ')
memBgi=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 4 | tr '\n' ' ')
memCac=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 5 | tr '\n' ' ')
memAva=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 6 | tr '\n' ' ')

swapTot=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 7 | tr '\n' ' ')
swapUse=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 8 | tr '\n' ' ')
swapFre=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 9 | tr '\n' ' ')

pathSize=$(ls /home/${user}/log | grep -Ev agg | sort | tail -n 60 | xargs -I {} cat /home/${user}/log/{} | cut -d "," -f 11 | tr '\n' ' ' | tr 'G' ' ')


IFS=' ' read -a memTotArr <<< "$memTot"
IFS=' ' read -a memUseArr <<< "$memUse"
IFS=' ' read -a memFreArr <<< "$memFre"
IFS=' ' read -a memBgiArr <<< "$memBgi"
IFS=' ' read -a memCacArr <<< "$memCac"
IFS=' ' read -a memAvaArr <<< "$memAva"

IFS=' ' read -a swapTotArr <<< "$swapTot"
IFS=' ' read -a swapUseArr <<< "$swapUse"
IFS=' ' read -a swapFreArr <<< "$swapFre"

IFS=' ' read -a pathSizeArr <<< "$pathSize"


avg() {
    local arr=("$@")
    local sum=0
    for item in "${arr[@]}"; do
        item_int=${item%.*}
        sum=$((sum+item_int))
    done
    avg=$((sum/60))
    echo "$avg"
}


min() {
    local arr=("$@")
    local min=0
    for item in "${arr[@]}"; do
        item_int=${item%.*}
        if [ "$item_int" -lt "$min" ]; then
            min=$item_int
        fi
    done
    echo "$min"
}


max() {
    local arr=("$@")
    local max=0
    for item in "${arr[@]}"; do
        item_int=${item%.*}
        if [ "$item_int" -gt "$max" ]; then
            max=$item_int
        fi
    done
    echo "$max"
}


minMemTot=$(min "${memTotArr[@]}")
maxMemTot=$(max "${memTotArr[@]}")
avgMemTot=$(avg "${memTotArr[@]}")

minMemUse=$(min "${memUseArr[@]}")
maxMemUse=$(max "${memUseArr[@]}")
avgMemUse=$(avg "${memUseArr[@]}")

minMemFre=$(min "${memFreArr[@]}")
maxMemFre=$(max "${memFreArr[@]}")
avgMemFre=$(avg "${memFreArr[@]}")

minMemBgi=$(min "${memBgiArr[@]}")
maxMemBgi=$(max "${memBgiArr[@]}")
avgMemBgi=$(avg "${memBgiArr[@]}")

minMemCac=$(min "${memCacArr[@]}")
maxMemCac=$(max "${memCacArr[@]}")
avgMemCac=$(avg "${memCacArr[@]}")

minMemAva=$(min "${memAvaArr[@]}")
maxMemAva=$(max "${memAvaArr[@]}")
avgMemAva=$(avg "${memAvaArr[@]}")


minSwapTot=$(min "${swapTotArr[@]}")
maxSwapTot=$(max "${swapTotArr[@]}")
avgSwapTot=$(avg "${swapTotArr[@]}")

minSwapUse=$(min "${swapUseArr[@]}")
maxSwapUse=$(max "${swapUseArr[@]}")
avgSwapUse=$(avg "${swapUseArr[@]}")

minSwapFre=$(min "${swapFreArr[@]}")
maxSwapFre=$(max "${swapFreArr[@]}")
avgSwapFre=$(avg "${swapFreArr[@]}")


minPathSize=$(min "${pathSizeArr[@]}")
maxPathSize=$(max "${pathSizeArr[@]}")
avgPathSize=$(avg "${pathSizeArr[@]}")


echo "minimum,$minMemTot,$minMemUse,$minMemFre,$minMemBgi,$minMemCac,$minMemAva,$minSwapTot,$minSwapUse,$minSwapFre,$checkPath,${minPathSize}G" >> $path/metrics_agg_$timestamp.log
echo "maximum,$maxMemTot,$maxMemUse,$maxMemFre,$maxMemBgi,$maxMemCac,$maxMemAva,$maxSwapTot,$maxSwapUse,$maxSwapFre,$checkPath,${maxPathSize}G" >> $path/metrics_agg_$timestamp.log
echo "average,$avgMemTot,$avgMemUse,$avgMemFre,$avgMemBgi,$avgMemCac,$avgMemAva,$avgSwapTot,$avgSwapUse,$avgSwapFre,$checkPath,${avgPathSize}G" >> $path/metrics_agg_$timestamp.log

chmod 600 $path/metrics_agg_$timestamp.log