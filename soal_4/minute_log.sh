#!/bin/bash

timestamp=$(date "+%Y%m%d%H%M%S")
user=$(whoami)
path="/home/${user}/log"

memTot=$(free -m | grep Mem | awk '{print $2}')
memUse=$(free -m | grep Mem | awk '{print $3}')
memFre=$(free -m | grep Mem | awk '{print $4}')
memBgi=$(free -m | grep Mem | awk '{print $5}')
memCac=$(free -m | grep Mem | awk '{print $6}')
memAva=$(free -m | grep Mem | awk '{print $7}')

swapTot=$(free -m | grep Swap | awk '{print $2}')
swapUse=$(free -m | grep Swap | awk '{print $3}')
swapFre=$(free -m | grep Swap | awk '{print $4}')

checkPath="/home/${user}"
sizePath=$(du -sh ${checkPath}| awk '{print $1}')

echo $memTot,$memUse,$memFre,$memBgi,$memCac,$memAva,$swapTot,$swapUse,$swapFre,$checkPath,$sizePath > ${path}/metrics_${timestamp}.log

chmod 600 ${path}/metrics_${timestamp}.log