#!/bin/bash

wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp' -O 'playlist.csv'
awk -F ',' '$4 ~/hip hop/ {print $15, $4, $2}' playlist.csv| sort -k1,1nr | head -n 5
awk -F ',' '$3 == John Mayer" {print $15, $3, $2}' playlist.csv| sort -k1,1nr | head -n 5
awk -F ',' '$5 =="2004" {print $15, $5, $2}' playlist.csv| sort -k1,1nr | head -n 10
grep "Sri" playlist.csv | cut -d ',' -f 3,2
