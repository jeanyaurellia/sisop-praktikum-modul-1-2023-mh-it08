#!/bin/bash

user=$(whoami)
path=/home/${user}/users
time=$(date +"%Y/%m/%d %H:%M:%S")

echo "Masukkan Email: "
read email

echo "Masukkan Password: "
read passwordInput

dpasswordInput=$(echo -n "$passwordInput" | base64)

emailData=$(cat $path/users.txt | cut -d "," -f 1 | tr '\n' ' ')

IFS=' ' read -a emailArr <<< "$emailData"

emailExist=false

for item in "${emailArr[@]}"
do 
    if [ "$item" == "$email" ]
    then
        password=$(cat $path/users.txt | grep $item | cut -d "," -f 3)
        username=$(cat $path/users.txt | grep $item | cut -d "," -f 2)
        emailExist=true
    fi
done

pwRight=false

if [ "$password" == "$dpasswordInput" ]
then
    pwRight=true
fi

if $emailExist && $pwRight
then
    echo "LOGIN SUCCESS - Welcome, $username"
    echo "[$time] [LOGIN SUCCESS] user $username login successfully" >> $path/auth.log
else
    if ! $emailExist
    then
        echo "LOGIN FAILED - email $email not registered, please register first"
        echo "[$time] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> $path/auth.log
    fi
    if ! $pwRight
    then
        echo "LOGIN FAILED - the password is wrong, please try again"
        echo "[$time] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> $path/auth.log
    fi
fi

