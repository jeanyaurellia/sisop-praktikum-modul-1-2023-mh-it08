#!/bin/bash

user=$(whoami)
path=/home/${user}/users
time=$(date +"%Y/%m/%d %H:%M:%S")

touch $path/users.txt
touch $path/auth.log

masuk=true


while $masuk
do
    echo "Masukkan email: "
    read email

    echo "Masukkan username: "
    read username

    echo "Masukkan password: "
    read password

    password=$(echo "$password" | tr -d '[:space:]')

    emailData=$(cat $path/users.txt | cut -d "," -f 1 | tr '\n' ' ')

    IFS=' ' read -a emailArr <<< "$emailData"

    emailTrue=true

    for item in "${emailArr[@]}"
    do 
        if [ "$item" == "$email" ]
        then
            echo "Email sudah terdaftar"
            emailTrue=false
            break
        fi
    done

    pw=true

    if [[ ${#password} -lt 8 ]]; then
        echo "Password harus tepat 8 karakter"
        pw=false
    fi

    if [[ ! ${password} =~ [A-Z] ]]; then
        echo "Password tidak memiliki huruf besar"
        pw=false
    fi

    if [[ ! ${password} =~ [a-z] ]]; then
        echo "Password tidak memiliki huruf kecil"
        pw=false
    fi

    if [[ ! ${password} =~ [0-9] ]]; then
        echo "Password tidak memiliki angka"
        pw=false
    fi

    if [[ ! ${password} =~ [^a-zA-Z0-9\s] ]]; then
        echo "Password tidak memiliki special characters"
        pw=false
    fi


    if $emailTrue
    then
        echo "Email Teregistrasi"
    fi

    if $emailTrue && $pw 
    then
        encoded_password=$(echo -n "$password" | base64)
        echo "$email,$username,$encoded_password" >> $path/users.txt
        echo " " >> $path/users.txt
        echo "[$time] [REGISTER SUCCESS] user $username registered successfully" >> $path/auth.log
        echo "Berhasil Registrasi"
        masuk=false
    else
        echo "Password tidak sesuai ketentuan"
        echo "[$time] [REGISTER FAILED] ERROR Failed register attempt on user" >> $path/auth.log
    fi
done